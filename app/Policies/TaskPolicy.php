<?php

namespace App\Policies;

use App\User;
use App\Task;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;
    protected $permittedUserMails;

    public function __construct()
    {
        //
        $admins = User::whereHas(
            'roles', function($q){
                $q->where('name', 'admin');
            }
        )->get();

        $managers = User::whereHas(
            'roles', function($q){
                $q->where('name', 'manager');
            }
        )->get();
        // Only Admins and managers 
        $this->permittedUserMails = collect([$admins, $managers])->collapse()->map(function ($user)
        {
            return $user->email;
        })->all();

    }

    /**
     * Determine whether the user can view any tasks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function view(User $user, Task $task)
    {
        // 
        // dd($user->company_id);
        return $user->company_id == $task->company_id && ($user->id == $task->user->id || in_array($user->email, $this->permittedUserMails));

    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->email, $this->permittedUserMails);
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        //
        return $user->company_id == $task->company_id && ($user->id == $task->user->id || in_array($user->email, $this->permittedUserMails));
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        //
        return $user->company_id == $task->company_id && (in_array($user->email, $this->permittedUserMails));
    }

    /**
     * Determine whether the user can restore the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function restore(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function forceDelete(User $user, Task $task)
    {
        //
    }
}
