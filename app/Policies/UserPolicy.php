<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    protected $permittedUserMails;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $user = auth()->user();
        $admins = User::whereHas(
            'roles', function($q) use($user){
                $q->where('name', 'admin')->where('company_id', $user->company_id);
            }
        )->get();

        $managers = User::whereHas(
            'roles', function($q) use($user){
                $q->where('name', 'manager')->where('company_id', $user->company_id);
            }
        )->get();
        // Only Admins and managers 
        $this->permittedUserMails = collect([$admins, $managers])->collapse()->map(function ($user)
        {
            return $user->email;
        })->all();

        // dd($this->permittedUserMails);
    }

    /**
     * Determine whether the user can view any meetings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        $sameCompanyUsers = collect([User::where('company_id', $user->company_id)->get()])->collapse()->map(function($user){
            return $user->company_id;
        })->all();
        // dd($sameCompanyUsers);
        return in_array($user->company_id, $sameCompanyUsers);
    }

    /**
     * Determine whether the user can view any meetings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAdmin(User $user)
    {
        $admins = User::whereHas(
            'roles', function($q){
                $q->where('name', 'admin');
            }
        )->get();

        // Only Admins 
        $this->permittedAdminMails = collect([$admins])->collapse()->map(function ($user)
        {
            return $user->email;
        })->all();
        return in_array($user->email, $this->permittedAdminMails);
        // return true;
    }

    /**
     * Determine whether the user can view any meetings.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    // public function viewOwnTask(User $user, Task $task)
    // {
    //     // $hasTasks = \App\Task::where('user_id',$user->id)->get();
    //     // dd($h);
    //     return true;
    //     // return true;
    // }

}
