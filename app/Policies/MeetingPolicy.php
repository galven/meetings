<?php

namespace App\Policies;

use App\User;
use App\Meeting;
use Illuminate\Auth\Access\HandlesAuthorization;

class MeetingPolicy
{
    use HandlesAuthorization;

    private $permittedUserMails;

    public function __construct()
    {
        $admins = User::whereHas(
            'roles', function($q){
                $q->where('name', 'admin');
            }
        )->get();

        $managers = User::whereHas(
            'roles', function($q){
                $q->where('name', 'manager');
            }
        )->get();
        // Only Admins and managers 
        $this->permittedUserMails = collect([$admins, $managers])->collapse()->map(function ($user)
        {
            return $user->email;
        })->all();
    }
    /**
     * Determine whether the user can view any meetings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function view(User $user, Meeting $meeting)
    {
        //
        return $user->company_id == $meeting->company_id && in_array($user->email,$this->permittedUserMails);
    }

    /**
     * Determine whether the user can create meetings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->email,$this->permittedUserMails);
    }

    /**
     * Determine whether the user can update the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function update(User $user, Meeting $meeting)
    {
        //
        return in_array($user->email,$this->permittedUserMails);
    }

    /**
     * Determine whether the user can delete the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function delete(User $user, Meeting $meeting)
    {
        //
        return in_array($user->email,$this->permittedUserMails);
    }

    /**
     * Determine whether the user can restore the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function restore(User $user, Meeting $meeting)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function forceDelete(User $user, Meeting $meeting)
    {
        //
    }
}
