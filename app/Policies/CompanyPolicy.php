<?php

namespace App\Policies;

use App\User;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;


    protected $permittedUserMails;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        $admins = User::whereHas(
            'roles', function($q){
                $q->where('name', 'admin');
            }
        )->get();

        $managers = User::whereHas(
            'roles', function($q){
                $q->where('name', 'manager');
            }
        )->get();
        // Only Admins and managers 
        $this->permittedUserMails = collect([$admins, $managers])->collapse()->map(function ($user)
        {
            return $user->email;
        })->all();

    }
    /**
     * Determine whether the user can view any companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return false;
    }

    /**
     * Determine whether the user can view the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function view(User $user, Company $company)
    {
        // admin or manager in same Company
        $admins = User::whereHas(
            'roles', function($q) use($company){
                $q->where('name', 'admin')->where('company_id', $company->id);
            }
        )->get();

        $managers = User::whereHas(
            'roles', function($q) use($company){
                $q->where('name', 'manager')->where('company_id', $company->id);
            }
        )->get();
        // Flat array 
        $permittedCompanyUserMails = collect([$admins, $managers])->collapse()->map(function ($user)
        {
            return $user->email;
        })->all();
        
        return in_array($user->email, $permittedCompanyUserMails);
        
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return in_array($user->email, $this->permittedUserMails);
    }

    /**
     * Determine whether the user can update the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function update(User $user, Company $company)
    {
        return $user->company_id == $company->id && in_array($user->email, $this->permittedUserMails);
    }

    /**
     * Determine whether the user can delete the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function delete(User $user, Company $company)
    {
        //
    }

    /**
     * Determine whether the user can restore the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function restore(User $user, Company $company)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function forceDelete(User $user, Company $company)
    {
        //
    }
}
