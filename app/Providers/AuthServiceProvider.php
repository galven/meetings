<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        // Gate::define('admin', function($user){
        //     $roles = collect($user->roles)->map(function($role){
        //         return $role->name;
        //     })->all();
        //     // dd(in_array('admin', $roles));
        //     return in_array('admin', $roles);
        // });

        // Gate::define('manager', function($user){
        //     $roles = collect($user->roles)->map(function($role){
        //         return $role->name;
        //     })->all();
        //     return in_array('manager', $roles);
        // });

        // Gate::define('user-task', function($user, $task){
        //     // $roles = collect($user->roles)->map(function($role){
        //     //     return $role->name;
        //     // })->all();
            
        //     return $user->id == $task->user->id;
        // });
    }
}
