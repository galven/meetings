<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'company_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function task()
    {
       return $this->hasMany(Task::class);
    }

    public function meeting()
    {
        return $this->hasMany(Meeting::class);
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user');
        // return $this->hasMany(Role::class, 'role_user', 'user_id');
    }
    public function hasAnyRoles($roles)
    {       
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    public function meetings()
    {
        return $this->belongsToMany(Meeting::class, 'meeting_user');
    }
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function hasRole($role)
    {
        $isRole =  User::whereHas('roles', function($q) use($role){
            $q->whereRoleId($role);
        })->get();

        return $isRole;
    }
}
