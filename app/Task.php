<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // Scope Status
    // In progress
    // Done 
    // Not Done 
    // Missed Deadline
    // Other
    protected $guarded = [];
    protected $attributes = [
        'status' => 0
    ];
    public function setDeadlineAttribute($value)
    {
        $time = strtotime($value);
        $this->attributes['deadline'] = date('Y-m-d H:i:s', $time);
    }

    public function getDeadlineAttribute()
    {
        if (isset($this->attributes['deadline'])) { 
            return date("Y-m-d H:i:s", strtotime($this->attributes['deadline']));
        }
        // Nothing to return maybe when creating task
        return '';
    }

    public function getStatusAttribute($attribute)
    {
        return $this->statusOptions()[$attribute];
    }

    public function statusOptions()
    {
        return [
            0 => 'Not Assigned',
            1 => 'In Progress',
            2 => 'Done',
            3 => 'Missed Deadline',
        ];
    }

    public function statusColoring($val)
    {
        $colors = [
            'Not Assigned' => "secondary",
            'In Progress' => "warning",
            'Done' => "success",
            'Missed Deadline' => "danger",
        ];
        return $colors[$val];    
    }

    public function user()
    {
       return $this->belongsTo(User::class);
    }

    public function meeting()
    {
        return $this->belongsTo(Meeting::class);
    }
}
