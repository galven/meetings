<?php

namespace App\Http\Controllers;

use App\Meeting;
use App\Task;
use App\Participant;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index()
    {
        // Meetings and Tasks for current user.
        // 
        $user = auth()->user();
        // Get Meeting by participant and by the host combined into one array
        $meetings = collect([Meeting::whereHas('participants',function($q) use($user){
            $q->whereUserId($user->id);
        })->get() , 
        Meeting::whereHas('hosts',function ($q)use($user)
        {
            $q->whereId($user->id);
        })->get()])->collapse();
        // dd($meetings);
        // Get task by current user
        $tasks = Task::whereHas('user', function($q) use($user){
            $q->whereUserId($user->id);
        })->get();
        // dd($tasks);
        // Done tasks
        $done = Task::where('user_id', $user->id)->where('status', 2)->get()->count();
        // In progress
        $inprog = Task::where('user_id', $user->id)->where('status', 1)->get()->count();

        // Missed deadlines
        $missed = Task::where('user_id', $user->id)->where('status', 3)->get()->count();
        // dd($done);
        // Assigned
        $assigned = Task::where('user_id', $user->id)->get()->count();
        return view('dashboard.index', compact('meetings', 'tasks', 'done', 'inprog','missed', 'assigned'));
    }
}
