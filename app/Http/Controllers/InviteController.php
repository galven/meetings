<?php

namespace App\Http\Controllers;

use App\Invite;
use App\Mail\InviteCreated;
use App\Mail\UserCreatedMail;
use App\User;
use App\Role;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InviteController extends Controller
{

    public function __construct()
    {
        $this->authorize('view', User::class);
    }
    public function invite()
    {
        // show the user a form with an email field to invite a new user
        $companies = \App\Company::all();
        return view('invite', compact('companies'));
    }

    /**
     * @param \Illiminate\Http\Request $request
     */
    public function process(Request $request)
    {
        // process the form submission and send the invite by email
         // validate the incoming request data

        do {
            //generate a random string using Laravel's str_random helper
            $token = str_random();
        } //check if the token already exists and if it does, try again
        while (Invite::where('token', $token)->first());

        //create a new invite record
        $invite = Invite::create([
            'email' => $request->get('email'),
            'company_id' => (int) $request->input('company_code'),
            'token' => $token
        ]);

        // send the email
        Mail::to($request->get('email'))->send(new InviteCreated($invite));

        // redirect back where we came from
        return redirect()
            ->back();
    }

    public function accept($token)
    {
        // here we'll look up the user by the token sent provided in the URL
            // Look up the invite
        if (!$invite = Invite::where('token', $token)->first()) {
            //if the invite doesn't exist do something more graceful than this
            abort(404);
        }

        // create the user with the details from the invite
        $user = User::create(['name' => explode('@', $invite->email)[0],
                              'email' => $invite->email,
                              'password' => bcrypt($invite->email),
                              'email_verified_at' => date("Y-m-d H:i:s"),
                              'company_id' => $invite->company_id,
                ]);
        // Assign Admin role to user that is created from Invite 
        $user->roles()->attach([1,2]); // Admin, user

        // delete the invite so it can't be used again
        $invite->delete();

        // here you would probably log the user in and show them the dashboard, but we'll just prove it worked
        Mail::to($user->email)->send(new UserCreatedMail($user));
        return redirect('dashboard')->with('msg', 'Your credentials send to your mail.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function show(Invite $invite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function edit(Invite $invite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invite $invite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invite $invite)
    {
        //
    }
}
