<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', \App\User::class);

        $subjects = Subject::where('company_id', auth()->user()->company_id)->get();
        $meetings = \App\Meeting::where('company_id', auth()->user()->company_id)->get();
        return view('subjects.index', compact('subjects', 'meetings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view', \App\User::class);
        $subject = new Subject();
        $meetings = \App\Meeting::where('company_id', auth()->user()->company_id)->get();
        return view('subjects.create', compact('subject', 'meetings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('view', \App\User::class);
        $validated = $this->validateRequest();
        $validated['status'] = 0;
        $validated['company_id'] = auth()->user()->company_id;
        $subject = Subject::create($validated);
        return redirect('subjects');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
        $this->authorize('view', \App\User::class);
        return view('subjects.show', compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->authorize('view', \App\User::class);
        $subject = Subject::findOrFail($id);
        $meetings = \App\Meeting::where('company_id', auth()->user()->company_id)->get();
        
        return view('subjects.edit', compact('subject', 'meetings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        //
        $this->authorize('view', \App\User::class);
        $subject->update($this->validateRequest());
        return view('subjects.show', compact('subject'));
        
    }

    /**
     * Update only status and editable fields
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateStatus(Request $request, $id)
    {
       $this->authorize('view', \App\User::class);
        $validated = request()->validate([
           "status" => "required|integer",
           "editable" => "required|integer",
        ]);
        $subject = Subject::findOrFail($id);
        $subject->update($validated);
        return response()->json(['success'=> 'updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        //
        $this->authorize('view', \App\User::class);
        $subject->delete();
        return redirect('subjects');
    }

    public function validateRequest()
    {
        return request()->validate([
            'title' => 'required|min:5',
            "meeting_id" => "required|integer", 
            "status" => "integer",
        ]);   
    }
}
