<?php


namespace App\Http\Controllers;

use App\Task;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TasksController extends Controller
{
    private $meetings;
    public function __construct()
    {
        $this->meetings = \App\Meeting::all(); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        $tasks = Task::where('company_id', auth()->user()->company_id)->get();
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('create', \App\Task::class);
        $task = new Task();
        $user = auth()->user();
        $users = \App\User::where('company_id', auth()->user()->company_id)->get();
        $meetings = collect($this->meetings)->filter(function($value, $key)use($user){
            return $value->company_id == $user->company_id;
        });
        return view('tasks.create', compact('task', 'users','meetings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('create', \App\Task::class);
        $validated = $this->validateRequest();
        // Automatic company id assignement to creating users
        $validated['company_id'] = auth()->user()->company_id;
        $task = Task::create($validated);
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $this->authorize('view', auth()->user(),\App\Task::class);
        $task = Task::findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $user = auth()->user();
        $users = \App\User::where('company_id', $user->company_id)->get();
        $meetings = collect($this->meetings)->filter(function($value, $key)use($user){
            return $value->company_id == $user->company_id;
        });
        return view('tasks.edit', compact('task','users', 'meetings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $task = Task::findOrFail($id);
        $task->update($this->validateRequest());
        return redirect('tasks/'. $id);
    }

    /**
     * Update only status and editable fields
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function updateStatus(Request $request, $id)
     {
         $validated = request()->validate([
            "status" => "required|integer",
            "editable" => "required|integer",
         ]);
         $task = Task::findOrFail($id);
         $task->update($validated);
         return response()->json(['success'=> 'updated']);
     }
         /**
     * Update only status and user_id fields
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function assignUser(Request $request, $id)
    {
        $validated = request()->validate([
           "status" => "required|integer",
           "user_id" => "required|integer",
        ]);
        $task = Task::findOrFail($id);
        $task->update($validated);
        return response()->json(['success'=> 'User assigned']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return redirect('/tasks');
    }

    public function validateRequest()
    {
        return request()->validate([
            'description' => 'required|min:5',
            "deadline" => "required|date_format:Y-m-d H:i:s", //date_format:d/m/Y h:i A
            "user_id" => 'integer',
            "status" => "required",
            "meeting_id" => 'nullable',
        ]);
    }
}
