<?php

namespace App\Http\Controllers;

use  \App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $roles = \App\Role::all();
        return view('users.create', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $this->validateRequest();
            if($validated['role_id'] == "0"){
            $validated['role_id'] = 2;
        }
        $validated['company_id'] = auth()->user()->company_id;
        // Create hash for user password
        $validated['password'] = Hash::make($validated['password']);
        $user = User::create($validated);
        $user->roles()->attach([2]);
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
        $roles = \App\Role::all();
        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $user)
    {
        
        $validated = request()->validate([
            'name' => 'required|min:5',
            "email" => "required|email",
            'role_id' => 'integer',
        ]);

        if(!$request->password){
            $validated['password'] = $user->password; // Same password
        }else{ // Changed password
            $validated['password'] = Hash::make($request->password);
        }
        $user->update($validated);
        return view('users.show', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();

        return redirect('users');
    }

    public function validateRequest()
    {
        return request()->validate([
            'name' => 'required|min:5',
            "email" => "required|email",
            'role_id' => 'integer',
            "password" => 'required|min:8|confirmed', ///different:current_password|
        ]);
    }
}
