<?php

namespace App\Http\Controllers;

use App\Meeting;
use Illuminate\Http\Request;

class MeetingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', \App\User::class);
        $meetings = Meeting::where('company_id', auth()->user()->company_id)->get();
        return view('meetings.index', compact('meetings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view', \App\User::class);
        $meeting = new Meeting();
        $users = \App\User::where('company_id', auth()->user()->company_id)->get();
        $user = auth()->user();
        $managers = \App\User::whereHas('roles', function($q) use($user) {
            $q->whereRoleId(1)->where('company_id', $user->company_id);
        })->get();
        $host = auth()->user();
        return view('meetings.create', compact('meeting', 'users', 'host', 'managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Meeting $meetings
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Meeting $meeting)
    {
        //

        $this->authorize('create', Meeting::class);
        $validated = $this->validateRequest();
        
        $user = auth()->user();
        $validated['participant_id'] = 0; /// Change to Current user
        $validated['host'] = $user->id;  /// Change to Current user
        $validated['company_id'] = $user->company_id;
        $meeting = Meeting::create($validated);
        $participants = [];
        foreach ($request->participant_id as $key => $id) {
            $participants[] = [
                'user_id' => $id,
                'meeting_id' => $meeting->id
            ];
        }
        $participant = \App\Participant::insert($participants);

        return redirect('subjects/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function show(Meeting $meeting)
    {
        return view('meetings.show', compact('meeting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function edit(Meeting $meeting)
    {
        //
        $this->authorize('view', \App\User::class);
        $users = \App\User::where('company_id', auth()->user()->company_id)->get();
        $participants = \App\Participant::where('meeting_id', $meeting->id);
        $host = auth()->user();
        $managers = \App\User::whereHas('roles', function($q) use($host){
            $q->whereRoleId(1)->where('company_id', $host->company_id);
        })->get();
        return view('meetings.edit', compact('meeting','users', 'participants', 'host', 'managers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Meeting $meeting)
    {
        //
        $this->authorize('view', \App\User::class);
        $validated = $this->validateRequest();
        // Cut participants from the validated fields
        $participants = $validated['participant_id'];
        unset($validated['participant_id']);
        // Add participants for this meeting
        $meeting->participants()->sync($participants);
        // Update rest of the fields
        $meeting->update($validated);
        return view('meetings.show', compact('meeting'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meeting $meeting)
    {
        //
        $this->authorize('view', \App\User::class);
        $meeting->delete();
        return redirect('meetings');
    }

    public function validateRequest()
    {
        return request()->validate([
            'title' => 'required|min:5',
            'starts_at' => 'required|date_format:Y-m-d H:i:s',
            'finishs_at' => 'required|date_format:Y-m-d H:i:s|after:starts_at',
            'status' => 'integer',
            'participant_id' => 'required|array',
            'manager' => 'sometimes|integer',
        ]);   
    }
}
