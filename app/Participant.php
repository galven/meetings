<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    //
    public function meetings()
    {
        return $this->hasMany(Meeting::class);
    }
}
