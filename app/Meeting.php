<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    //
    protected $attributes = [
        'status' => 0
    ];
    protected $casts = [
        'starts_at' => 'datetme:Y-m-d H:i:s',
        'finishs_at' => 'datetme:Y-m-d H:i:s'
    ];

    protected $guarded = [];
    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }

    public function users()
    {
        return $this->hasMany(User::class, 'users');
    }
    public function hosts()
    {
        return $this->belongsTo(User::class,'host');
    }
    public function managers()
    {
        return $this->belongsTo(User::class, 'manager');
    }

    public function participants()
    {
        return $this->belongsToMany(User::class,'participants');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    
    public function getStatusAttribute($attribute)
    {
        return $this->statusOptions()[$attribute];
    }

    public function statusOptions()
    {
        return [
            0 => 'Up coming',
            1 => 'Passed',
            2 => 'Now',
        ];
    }

    public function setStartsAtAttribute($value)
    {
        $this->attributes['starts_at'] = date('Y-m-d H:i:s',strtotime($value));
    }
    public function getStartsAtAttribute($value)
    {
        
        $date = !empty($this->attributes) && isset($this->attributes['starts_at']) ?
                 date('Y-m-d H:i:s', strtotime($this->attributes['starts_at'])) : '';

        return $date;
    }

    public function setFinishsAtAttribute($value)
    {
        $this->attributes['finishs_at'] = date('Y-m-d H:i:s',strtotime($value));


    }
    public function getFinishsAtAttribute($value)
    {
        $date = !empty($this->attributes) && isset($this->attributes['finishs_at']) ?
                date('Y-m-d H:i:s', strtotime($this->attributes['finishs_at'])) : '';

        return $date;
    }
}
