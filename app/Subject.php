<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
    protected $guarded = [];
    protected $attributes = [
        'status' => 0
    ];
    public function meeting()
    {
        return $this->belongsTo(Meeting::class);
    }

    public function statusOptions()
    {
        return [
            0 => 'Not Done',
            1 => 'Done',
        ];
    }

    public function getStatusAttribute($attribute)
    {
        return $this->statusOptions()[$attribute];
    }

}
