<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');   
})->middleware('auth');



Route::get('invite', 'InviteController@invite')->name('invite')->middleware('auth');
Route::post('invite', 'InviteController@process')->name('process')->middleware('auth');
Route::get('accept/{token}', 'InviteController@accept')->name('accept');


Route::get('dashboard', 'DashboardController@index')->middleware('auth');
Route::resource('tasks', 'TasksController')->middleware('auth');
Route::post('tasks/updateStatus/{id}', 'TasksController@updateStatus')->name('tasks.updateStatus')->middleware('auth');
Route::post('tasks/assignUser/{id}', 'TasksController@assignUser')->name('tasks.assignUser')->middleware('auth');

Route::resource('meetings', 'MeetingController')->middleware('auth');
Route::resource('users', 'UsersController')->middleware('auth');
Route::resource('companies', 'CompanyController')->middleware('auth');
Route::resource('subjects', 'SubjectController')->middleware('auth');
Route::post('subjects/updateStatus/{id}', 'SubjectController@updateStatus')->name('subjects.updateStatus')->middleware('auth');

Auth::routes();
Route::get('register.admin', 'Auth\RegisterController@admin')->name('register.admin');
Route::post('register.createAdmin', 'Auth\RegisterController@createAdmin')->name('register.createAdmin');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
