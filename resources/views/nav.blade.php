<div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('/dashboard')}}">
                        <span data-feather="home"></span>
                        Dashboard <span class="sr-only">(current)</span>
                    </a>
                </li>
                @canany(['create'], App\Meeting::class)
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('meetings')}}">
                        <span data-feather="meetings"></span>
                        Meetings
                    </a>
                </li>
                
                <li class="nav-item">
                        <a class="nav-link" href="{{asset('subjects')}}">
                            <span data-feather="subjects"></span>
                            <i class="fa">&#xf0da;</i> Subjects
                        </a>
                </li>
                @endcanany 
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('tasks')}}">
                        <span data-feather="tasks"></span>
                        Tasks
                    </a>
                </li>
            </ul>
            {{-- @if(Auth::user()->hasRole("admin") || (Auth::user()->hasRole("manager"))) --}}
            <!-- Management Section of the Side bar -->
            @can('viewAdmin', App\User::class)
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>Management Panel</span>
                <a class="d-flex align-items-center text-muted" href="{{asset('#')}}">
                    <span data-feather="plus-circle"></span>
                </a>
            </h6>
            <ul class="nav flex-column mb-2">
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('users')}}">
                        <span data-feather="users"></span>
                        Users
                    </a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="{{asset('companies')}}">
                            <span data-feather="companies"></span>
                            Companies
                        </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="{{asset('invite')}}">
                        <span data-feather="Invite"></span>
                        Invite
                    </a>
                </li> --}}
                @endcan
                {{-- @endif --}}
            </ul>
        </div>
    </nav>
    <script type="application/javascript">
    $(function(){
        var active = window.location.href.split('/')[3]; 

        var found = $('.sidebar-sticky').find('active')
        if(found.length){
            found.removeClass('active');
        }
        $('.sidebar-sticky').find('span[data-feather="'+ (active ? active : 'home') +'"]').parent('.nav-link').addClass('active');
    })
    </script>