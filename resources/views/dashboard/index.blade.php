@extends('layouts.app')

@section('title', 'Task List')

@section('content')


<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  // Load Charts and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Draw the pie chart for Sarah's pizza when Charts is loaded.
      google.charts.setOnLoadCallback(drawSarahChart);

      // Draw the pie chart for the Anthony's pizza when Charts is loaded.
      google.charts.setOnLoadCallback(drawAnthonyChart);

      // Callback that draws the pie chart for Sarah's pizza.
      function drawSarahChart() {

        // Create the data table for Sarah's pizza.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['All Tasks', 1],
          ['Onions', 1],
          ['Olives', 2],
          ['Zucchini', 2],
          ['Pepperoni', 1]
        ]);

        // Set options for Sarah's pie chart.
        var options = {title:'How Successfull you are  among your friends :)',
                       width:400,
                       height:300};

        // Instantiate and draw the chart for Sarah's pizza.
       // var chart = new google.visualization.PieChart(document.getElementById('Sarah_chart_div'));
       // chart.draw(data, options);
      }

      // Callback that draws the pie chart for Anthony's pizza.
      function drawAnthonyChart() {

        // Create the data table for Anthony's pizza.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          // ['Assigned', {{-- $assigned --}}], // Assigned
          ['Done', {{ $done }}], // Here done 
          ['In progress', {{ $inprog }}], // progress
          ['Missed Deadline', {{ $missed }}],
        ]);

        // Set options for Anthony's pie chart.
        var options = {title:'How much have you done :)',
                       width:600,
                       height:300};

        // Instantiate and draw the chart for Anthony's pizza.
        var chart = new google.visualization.PieChart(document.getElementById('Anthony_chart_div'));
        chart.draw(data, options);
      }
</script>

<div class="row py-3">
  <h3>Statistics</h3>
  <div class="col-sm">
  User:
  <strong>{{ auth()->user()->name }}</strong>
  Company:
  <strong>{{ auth()->user()->company->name }}</strong>
  </div>
  <!--Div that will hold the pie chart-->
  <div class="col-sm-12">
    <table class="columns">
      <tr>
        <td>
          <div id="Sarah_chart_div" style="border: 1px solid #ccc"></div>
        </td>
        <td>
          <div id="Anthony_chart_div" style="border: 1px solid #ccc"></div>
        </td>
      </tr>
    </table>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-6">
    <h4>Meetings</h4>
    <ul class="list-group">
      @foreach ($meetings as $meeting)
      <li class="list-group-item"><a href="{{route('meetings.show',['meeting'=>$meeting])}}">{{$meeting->title}}</a>
      </li>
      @endforeach
    </ul>
  </div>
  <div class="col-6">
    <h4>Tasks</h4>
    <ul class="list-group">
      {{-- <li class="list-group-item">
          <span class="alert alert-success">
              <strong>Update!</strong>
              Task marked as done!
            </span>
      </li> --}}
      @foreach ($tasks as $task)
      <li class="list-group-item">
        <div class="custom-control custom-checkbox">
          <input type="hidden" name="id" id="id" value="{{$task->id}}">
          <input type="checkbox" class="custom-control-input updateStatus" 
                  id="updateStatusId_{{$task->id}}" 
                  {{$task->editable ? '' : 'disabled checked'}}>
        <label class="custom-control-label" for="updateStatusId_{{$task->id}}"
          {{$task->editable ? '' : 'style=text-decoration:line-through'}}>{{$task->description}}</label>
        </div>
      </li>

      @endforeach
    </ul>
  </div>
</div>

<script type="application/javascript">
  $(document).ready(function(){
    
    console.log('ready to work')
     $('.updateStatus').on('click', function(){
        var chk = $(this).parent();
        var txt = chk.find('#id');
        chk.find('label').css({'text-decoration': 'line-through'})
        chk.find('input[type="checkbox"]').attr('disabled', true)
       console.log('Clicked: ' +'/tasks/' + txt.val());
        //**********************
        // Update task to 
        // * editable => 0 // Cann't be edited anymore
        // * status => done
        //**********************
        $.ajax({
          url: "{{ asset('/tasks/updateStatus') }}/" + txt.val(),
          type: 'POST',
          headers: {
            "X-CSRF-TOKEN": "{{ csrf_token() }}",
          },
          data: {
            'editable': 0,
            'status': 2
          },
          success: function(d){
            console.log(d);
            // window.location.href = "tasks";
          },
          error: function(d){
            console.log(d);
            // window.location.href = "tasks";
          }
        });
     }) // Update Task status 
     
  });
</script>
@endsection