@extends('layouts.app')

@section('title', 'Edit Meeting')

@section('content')

<div class="row py-3">
    <h1> Edit Meeting </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('meetings.update', ['meeting'=> $meeting ]) }}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @include('meetings.form')
        <button type="submit" class="btn btn-success">Update</button>
    <a href="{{url('meetings')}}" class="btn btn-link">Cancel</a>
    </form>
</div>

@endsection