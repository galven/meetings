@extends('layouts.app')

@section('title', 'Meeting List')

@section('content')

<div class="row py-3">
  <h1> Meetings List </h1>
  <hr>
  <a href="{{asset('/meetings/create')}}" role="button" class="btn btn-success btn-lg">Add meeting</a>
</div>
<div class="row">
  <table class="table  table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Status</th>
        <th scope="col">Host</th>
        <th scope="col">Manager</th>
        <th scope="col">Starts At</th>
        <th scope="col">Finishes At</th>
        <th scope="col">Manage</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($meetings as $meeting)
      <tr>
        <th scope="row"> {{ $meeting->id}} </th>
        <td>
          {{$meeting->title}}
        </td>
        <td>
          {{ $meeting->status}}
        </td>
        <td>
          {{$meeting->hosts->name}}
        </td>
        <td>
          {{isset($meeting->managers)? $meeting->managers->name : 'No manager'}}
        </td>
        <td>
          {{ $meeting->starts_at }}
        </td>
        <td>
          {{  $meeting->finishs_at  }}
        </td>
        <td>
          <a class="btn btn-info" href="meetings/{{ $meeting->id }}" role="button">
            <i class="fa fa-eye"></i>
          </a>

          <a class="btn btn-success" href="meetings/{{ $meeting->id }}/edit" role="button">
            <i class="fa fa-pencil"></i>

          </a>
          <button class="btn btn-danger btn-sm deleteMe" data-meeting-id="{{ $meeting->id }}" role="button">
            <i class="fa fa-trash"></i>
          </button>

        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <script type="application/javascript">
    $(document).ready(function(){
       $('.deleteMe').on('click', function(){
         console.log('Clicked: ' +'/meetings/' + $(this).attr('data-meeting-id'));
          $.ajax({
            url: "{{url('meetings')}}/" + $(this).attr('data-meeting-id'),
            type: 'DELETE',
            headers: {
              "X-CSRF-TOKEN": "{{ csrf_token() }}",
            },
            success: function(){
              window.location.href = "meetings";
            },
            error: function(){
              window.location.href = "meetings";
            }
          });
       })
    });
  </script>
</div>

@endsection