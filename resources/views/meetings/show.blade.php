@extends('layouts.app')

@section('title', 'Meeting')

@section('content')

<div class="row py-3">
    <h1> Details for meeting #{{ $meeting->id}} </h1>
    <hr>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Details for meeting #{{ $meeting->id}}
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Meeting title</strong>
                    {{ $meeting->title }}
                </li>
                <li class="list-group-item">
                    {{-- List of all participants --}}
                    <strong>Meeting participants</strong>
                    <p>
                        <ul>
                            @foreach ($meeting->participants as $participant)
                            <li>{{$participant->name}}</li>
                            @endforeach
                        </ul>
                    </p>
                    {{-- {{ $meeting->user_id}} --}}
                </li>
                <li class="list-group-item">
                    <strong>Meeting host</strong>
                    {{ $meeting->host}}
                </li>
                <li class="list-group-item">
                    {{-- Delete me --}}
                    <strong>Meeting manager</strong>
                    {{ $meeting->manager}}
                </li>
                <li class="list-group-item">
                    <strong>Status</strong>
                    {{ $meeting->status }}
                </li>

                <li class="list-group-item">
                    <strong>Starts at</strong>
                    {{ $meeting->starts_at }}
                </li>
                <li class="list-group-item">
                    <strong>Finishs at</strong>
                    {{ $meeting->finishs_at }}
                </li>
                <li class="list-group-item">
                    <ul class="list-group list-group-flush">
                        <strong>Subjects for this meeting:</strong>
                        @foreach ($meeting->subjects as $subject)
                        <li class="list-group-item">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="id" id="id" value="{{$subject->id}}">
                                <input type="checkbox" class="custom-control-input updateSubjectStatus" id="updateStatusId_{{$subject->id}}">
                                <label class="custom-control-label" for="updateStatusId_{{$subject->id}}">{{$subject->title}}</label>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </li>
                <li class="list-group-item">
                    <ul class="list-group list-group-flush">
                        <strong>Task for this meeting:</strong>
                        @foreach ($meeting->tasks as $task)
                        <li class="list-group-item">
                            {{$task->description}} &nbsp;
                            @if($task->editable)
                            <input type="hidden" name="task_id" id="task_id" value="{{$task->id}}">
                            <select name="user_id" id="user_id" class="browser-default custom-select col-4 assignUser" >
                               <option value="0">Select participant for the task</option>
                                @foreach ($meeting->participants as $participant)
                            <option value="{{$participant->id}}"
                                {{$task->user_id == $participant->id ? 'selected': ''}}>
                                {{$participant->name}}
                            </option>
                                @endforeach
                            </select>
                            @else
                                <span class="badge badge-success">
                                    {{$task->status}}
                                </span>
                            @endif
                        </li>
                        @endforeach

                    </ul>
                </li>
            </ul>
            <div class="card-footer">
                @can('create', \App\Meeting::class)
                <a href="{{url('meetings')}}" class="btn btn-link pull-left" role="button">Cancel</a>
                @endcan
                @cannot('create', \App\Meeting::class)
                <a href="{{url('dashboard')}}" class="btn btn-link pull-left" role="button">Cancel</a>
                @endcan
                @if(!$meeting->show)
                <a href="{{ route('meetings.edit', ['meeting'=> $meeting ]) }}" class="btn btn-success pull-left"
                    role="button">
                    <i class="fa fa-pencil"></i>
                </a>

                <form class="form-inline" action="{{ route('meetings.destroy', ['meeting'=> $meeting ]) }}"
                    method="post" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
                @endif
            </div>
        </div>

    </div>
</div>

<script type="application/javascript">
    $(document).ready(function(){
      
      console.log('ready to work')
       $('select#user_id').on('change', function(){
         var tsk = $(this).parent();
         var sel = $(this);
         var tski = tsk.find('input#task_id').val();
         console.log('Clicked: ' +'/tasks/' + tski);

          $.ajax({
            url: "{{ asset('/tasks/assignUser') }}/" + tski,
            type: 'POST',
            headers: {
              "X-CSRF-TOKEN": "{{ csrf_token() }}",
            },
            data: {
              'user_id': sel.val(),
              'status': 1
            },
            success: function(d){
              console.log(d);
            },
            error: function(d){
              console.log(d);
            }
          });
       }) 
       
       $('.updateSubjectStatus').on('click', function(){
            
            var parent = $(this).parent()
            var chk = parent.find('#id')
            var txt = parent.find('label');
            console.log('Subj: ' + chk.val());
            txt.css({'text-decoration':'line-through'})
            parent.find('input[type="checkbox"]').attr('disabled',true);

          $.ajax({
            url: "{{ asset('/subjects/updateStatus') }}/" + chk.val(),
            type: 'POST',
            headers: {
              "X-CSRF-TOKEN": "{{ csrf_token() }}",
            },
            data: {
              'editable': 0,
              'status': 1 
            },
            success: function(d){
              console.log(d);
            },
            error: function(d){
              console.log(d);
            }
          });

       });
       
    });
  </script>

@endsection