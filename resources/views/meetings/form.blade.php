{{-- DatePicker https://tempusdominus.github.io/bootstrap-4/Usage/ --}}


@foreach ($errors->all() as $message)
<div class="alert alert-danger">
    <strong>Error!</strong>
    {{ $message }}
</div>
@endforeach
<div class="form-group">
    <label for="title">Meeting title</label>
    <input type="text" class="form-control" name="title" id="title" value="{{ old('title') ?? $meeting->title }}"
        autofocus>
    @if($errors->first('title'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('title')}}
    </div>
    @endif
</div>
<div class="form-group">
    <label for="host">Meeting Host</label>
    {{-- {{dd($host)}} --}}
    <input type="text" class="form-control" name="host" id="host" value="{{ old('host') ?? $host->name }}" readonly>
    @if($errors->first('host'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('host')}}
    </div>
    @endif
</div>
<div class="form-group">
    <label for="manager">Meeting Manager</label>
{{-- {{dd($meeting)}} --}}
    <select name="manager" id="manager" class="form-control custom-select">
        <option value="0">Select Manager</option>
        @foreach ($managers as $manager)
        <option value="{{$manager->id}}" {{old('manager') == $manager->id || $meeting->manager == $manager->id ? 'selected':''}}>
            {{$manager->name}}
        </option>
        @endforeach
    </select>
    @if($errors->first('manager'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('manager')}}
    </div>
    @endif
</div>

<div class="form-group">
    <label for="participant_id" title="(CTRL+Click to select multiple Users)">Meeting Participants (CTRL+Click to select multiple Users)</label>
    <select name="participant_id[]" id="participant_id" multiple class="form-control custom-select">
        @foreach ($users as $user)
        {{-- {{dd(collect($meeting->participants))}} --}}
        <option value="{{ $user->id }}" 
            {{collect(old('participant_id'))->contains($user->id)
                 ||  collect($meeting->participants)->map(function($u){return $u->id;})->contains($user->id) ? 'selected':''}}>
            {{$user->name}}
        </option>
        @endforeach
    </select>
    {{-- <input type="text" class="form-control" 
               name="users" id="users" 
               value="{{ old('users', 'Will be SELECT if there are any') ?? $meeting->users }}" readonly> --}}
    @if($errors->first('participant_id'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('participant_id')}}
    </div>
    @endif
</div>

<div class='form-group'>
    <div class="form-group">
        <label for="starts_at">Starts At</label>
        <div class="input-group date" data-target-input="nearest">
            <input type="text" class="form-control datetimepicker-input" name="starts_at" id="starts_at"
                data-target="#starts_at" value="{{ old('starts_at') ?? $meeting->starts_at }}" />
            <div class="input-group-append" data-target="#starts_at" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>
</div>
<div class='form-group'>
    <div class="form-group">
        <label for="finishs_at">Finishes At</label>
        <div class="input-group date" data-target-input="nearest">
            <input type="text" class="form-control datetimepicker-input" name="finishs_at" id="finishs_at"
                data-target="#finishs_at" value="{{ old('finishs_at') ?? $meeting->finishs_at }}" />
            <div class="input-group-append" data-target="#finishs_at" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="subject-counter">
        <label for="">Maximum subjects per meeting:</label>
        <p class="form-control">
            5
        </p>
    </div>
</div>
@if(!Request::is('*/create') && !Request::is('*/edit'))
<div class="form-group">
    <label for="mail_domain">Choose meeting</label>
    <select name="meeting_id" id="meeting_id" class="form-control custom-select">
        <option value="0">Select Meeting</option>
        @foreach ($meetings as $meeting)
        <option value="{{ $meeting->id }}"
            {{ isset($meeting->meeting) && $meeting->meeting->id === $meeting->id ? 'selected' : ''}}>
            {{ $meeting->title}} </option>
        @endforeach
    </select>
    @if($errors->first('meeting'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('meeting')}}
    </div>
    @endif
</div>
@endif
@if(Request::is('*/edit'))
<div class="form-group">
    <label for="registration_key">meeting Status</label>
    <select name="status" id="status" class="form-control custom-select">
        <option value="" disabled>Select meeting status</option>
        @foreach($meeting->statusOptions() as $statusOptionKey => $statusOptionValue)
        <option value="{{ $statusOptionKey }}" {{ $meeting->status === $statusOptionValue ? 'selected' : '' }}>
            {{ $statusOptionValue }}</option>
        @endforeach
    </select>
    </select>
</div>
@endif


@csrf

<script type="text/javascript">
    $(function () {
       $('#starts_at').datetimepicker({
        format:'Y-m-d H:i:s'
       });
       $('#finishs_at').datetimepicker({
        format:'Y-m-d H:i:s'
       });
    });
</script>