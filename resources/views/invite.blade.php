@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row py-3">
        <h1> Create Invite </h1>
        <hr>
    </div>
    <div class="row">
        <form action="{{ route('invite') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="email">
                    Email:
                </label>
                <input type="email" name="email" class="form-control" />
            </div>
            <div class="form-group">
                    <label for="company_code">
                        Companies:
                    </label>
                    <select name="company_code" id="company_code" class="form-control custom-select">
                        @foreach ($companies as $company)
                    <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success form-control">Send invite</button>
            </div>
        </form>
    </div>
</div>

@endsection