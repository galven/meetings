@extends('layouts.app')

@section('title', 'user')

@section('content')

<div class="row py-3">
    <h1> Details for user #{{ $user->id}} </h1>
    <hr>
</div>

<div class="row">
    <div class="col-12">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Details for user #{{ $user->id}}
                </div>

                <ul class="list-group list-group-flush">
                    <li class="list-group-item"> <strong>User name</strong>
                        {{ $user->name }}
                    </li>
                    <li class="list-group-item"> <strong>User mail</strong>
                        {{ $user->email }}
                    </li>
                    <li class="list-group-item">
                        <strong>User Roles</strong>
                        @foreach ($user->roles as $role)
                        <h5>
                            <span class="badge badge-primary">{{ $role->name}} </span>
                        </h5>
                        @endforeach
                    </li>
                    <li class="list-group-item">
                        <strong>Company</strong>
                        {{ isset($user->company) ? $user->company->name : 'No company'}}
                    </li>
                </ul>
                <div class="card-footer">
                    <a href="{{url('users')}}" class="btn btn-link pull-left" role="button">Back</a>
                    <a href="{{ route('users.edit', ['user'=> $user ]) }}" class="btn btn-success pull-left"
                        role="button">
                        <i class="fa fa-pencil"></i>
                    </a>

                    <form class="form-inline" action="{{ route('users.destroy', ['user'=> $user ]) }}" method="post"
                        enctype="multipart/form-data">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection