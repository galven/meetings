@extends('layouts.app')

@section('title', 'Create New User')

@section('content')

<div class="row py-3">
    <h1> Create New User </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
        @include('users.form')
        <button type="submit" class="btn btn-success">Create</button>
    </form>
</div>

@endsection