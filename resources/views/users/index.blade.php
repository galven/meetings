@extends('layouts.app')

@section('title', 'User List')

@section('content')

<div class="row py-3">
  <h1> Users List </h1>
  <hr>
  <a href="{{asset('/users/create')}}" role="button" class="btn btn-success btn-lg">Add user</a>
</div>
<div class="row">
  <table class="table  table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">User</th>
        <th scope="col">Role</th>
        <th scope="col">Company</th>
        <th scope="col">Manage</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
      @if(auth()->user()->company_id == $user->company_id)
      {{-- @can('view', $user, \App\User::class) --}}
      <tr>
        <th scope="row"> {{$user->id}} </th>
        <td>
          {{$user->name}}
        </td>
        <td>
          @foreach ($user->roles as $role)
        <span style="font-size:.875em" class="badge badge-primary">{{$role->name}}</span>
          @endforeach
        </td>
        <td>
          {{isset($user->company) ? $user->company->name : 'No company'}}
        </td>
        <td>
            <a class="btn btn-info" href="users/{{ $user->id }}" role="button">
              <i class="fa fa-eye"></i>
            </a>
          <a class="btn btn-success" href="users/{{ $user->id }}/edit" role="button">
            <i class="fa fa-pencil"></i>

          </a>
        <button class="btn btn-danger btn-sm deleteMe" data-user-id="{{ $user->id }}" role="button">
            <i class="fa fa-trash"></i>
          </button>

        </td>
      </tr>
      @endif
      @endforeach
    </tbody>
  </table>

  <script type="application/javascript">
    $(document).ready(function(){
      console.log('ready to work')
       $('.deleteMe').on('click', function(){
         console.log('Clicked: ' +'/users/' + $(this).attr('data-user-id'));
          $.ajax({
            url: "{{url('users')}}/" + $(this).attr('data-user-id'),
            type: 'DELETE',
            headers: {
              "X-CSRF-TOKEN": "{{ csrf_token() }}",
            },
            success: function(){
              window.location.href = "users";
            },
            error: function(){
              window.location.href = "users";
            }
          });
       })     
    });
</script>
</div>

@endsection