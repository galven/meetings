@foreach ($errors->all() as $message)
<div class="alert alert-danger">
    <strong>Error!</strong>
    {{ $message }}
</div>
@endforeach
<div class="form-group">
    <label for="name">User name</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') ?? $user->name }}" autofocus>
    @if($errors->first('name'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('name')}}
    </div>
    @endif
</div>
{{-- @if(Request::is('*/create')) --}}
<div class="form-group">
    <label for="role_id">User Role</label>
    <select name="role_id" id="role_id" class="form-control custom-select">
        <option value="0">Select Role for user</option>
        @foreach ($roles as $role)
    <option value="{{old('role_id') ?? $role->id}}"
         {{ old('role_id')  == $role->id || $user->role_id == $role->id ? 'selected': ''}}>{{ $role->name}}</option>
            
        @endforeach
    </select>
</div>
{{-- @endif --}}
<div class="form-group">
    <label for="registration_key">Registration Key</label>
    <p class="form-control">
        {{isset($user->company)?$user->company->registration_link:'No link here'}}
    </p>
</div>

<div class="form-group">
        <label for="email">Registration mail</label>
        <input type="email" class="form-control" name="email"
value="{{ old('email')?? $user->email }}"
        />
    </div>
@if(Request::is('*users/create') || Request::is('*users/*/edit'))
    <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control" type="password"
                name="password" id="password"
            />
    </div>
    <div class="form-group">
            <label for="password-confirm">Password Confirm</label>
            <input class="form-control" type="password"
                name="password_confirmation" id="password-confirm"
            />
    </div>
@endif


@csrf