@extends('layouts.app')

@section('title', 'Edit Users')

@section('content')

<div class="row py-3">
    <h1> Edit Users </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('users.update', ['user'=> $user ]) }}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @include('users.form')
        <button type="submit" class="btn btn-success">Update</button>
    <a href="{{url('users')}}" class="btn btn-link">Cancel</a>
    </form>
</div>

@endsection