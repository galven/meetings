@foreach ($errors->all() as $message)
<div class="alert alert-danger">
    <strong>Error!</strong>
    {{ $message }}
</div>
@endforeach
<div class="form-group">
    <label for="name">Company name</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') ?? $company->name }}" autofocus>
    @if($errors->first('name'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('name')}}
    </div>
    @endif
</div>

<div class="form-group">
    <label for="mail_domain">Company mail domain</label>
    <input type="email" name="mail_domain" id="mail_domain" class="form-control" value="{{old('mail_domain') ?? $company->mail_domain}}"/>
    @if($errors->first('mail_domain'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('mail_domain')}}
    </div>
    @endif
</div>
<div class="form-group">
    <label for="registration_key">Registration Key Auto Generated</label>
    <p class="form-control">
            {{old('registration_link') ?? $company->registration_link}}
    </p>
</div>


@csrf