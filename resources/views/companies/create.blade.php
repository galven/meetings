@extends('layouts.app')

@section('title', 'Create New Company')

@section('content')

<div class="row py-3">
    <h1> Create New company </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('companies.store') }}" method="post" enctype="multipart/form-data">
        @include('companies.form')
        <button type="submit" class="btn btn-success">Create</button>
    </form>
</div>

@endsection