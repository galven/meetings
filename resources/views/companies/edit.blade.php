@extends('layouts.app')

@section('title', 'Edit Company')

@section('content')

<div class="row py-3">
    <h1> Edit Company </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('companies.update', ['company'=> $company ]) }}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @include('companies.form')
        <button type="submit" class="btn btn-success">Update</button>
    <a href="{{url('companies')}}" class="btn btn-link">Cancel</a>
    </form>
</div>

@endsection