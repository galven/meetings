@extends('layouts.app')

@section('title', 'Company Details')

@section('content')

<div class="row py-3">
  <h1> Companies Details </h1>
  <hr>
</div>
<div class="row">
  <table class="table  table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Company</th>
        <th scope="col">Registration Link</th>
        <th scope="col">Manage</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($companies as $company)
      @can('view',$company)
      <tr>
        <th scope="row"> {{$company->id}} </th>
        <td>
          {{$company->name}}
        </td>
        <td title="Click to copy">
          <input type="text" class="selectLink"
              value="{{$company->registration_link}}" readonly/> &nbsp;
          <span class="badge badge-success copyLink" data-company-link="{{$company->registration_link}}">
              Click to copy
          </span>
        </td>
        <td>
            <a class="btn btn-info" href="companies/{{ $company->id }}" role="button">
              <i class="fa fa-eye"></i>
            </a>

          <a class="btn btn-success" href="companies/{{ $company->id }}/edit" role="button">
            <i class="fa fa-pencil"></i>

          </a>
        <button class="btn btn-danger btn-sm deleteMe" data-company-id="{{ $company->id }}" role="button">
            <i class="fa fa-trash"></i>
          </button>

        </td>
      </tr>
      @endcan
      @endforeach
    </tbody>
  </table>

  <script type="application/javascript">
    $(document).ready(function(){
       $('.deleteMe').on('click', function(){
         console.log('Clicked: ' +'/companies/' + $(this).attr('data-company-id'));
          $.ajax({
            url: "{{url('companies')}}/" + $(this).attr('data-company-id'),
            type: 'DELETE',
            headers: {
              "X-CSRF-TOKEN": "{{ csrf_token() }}",
            },
            success: function(){
              window.location.href = "companies";
            },
            error: function(){
              window.location.href = "companies";
            }
          });
       })
       $('.copyLink').on('click',function(){
          var link = $(this).parent().find('.selectLink')
          link.focus();
          link.select();
          document.execCommand('copy');
          var txt = $(this).text() // Click to copy
          $(this).removeClass('badge-success').addClass('badge-light')
          $(this).text(txt.replace('copy', 'copied'))
       })    
    });
</script>
</div>

@endsection