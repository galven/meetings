@extends('layouts.app')

@section('title', 'Company')

@section('content')

<div class="row py-3">
    <h1> Details for company #{{ $company->id}} </h1>
    <hr>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Details for company #{{ $company->id}}
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Company name</strong>
                    {{ $company->name }}
                </li>
                <li class="list-group-item">
                    <strong>Registration link</strong>
                    {{ old('registration_link')??$company->registration_link}}
                </li>
                <li class="list-group-item">
                    <strong>Company</strong>
                    {{ old('mail_domain') ?? $company->mail_domain }}
                </li>
            </ul>
            <div class="card-footer">
            <a href="{{url('companies')}}" class="btn btn-link pull-left" role="button">Cancel</a>
                <a href="{{ route('companies.edit', ['company'=> $company ]) }}" class="btn btn-success pull-left"
                    role="button">
                    <i class="fa fa-pencil"></i>
                </a>

                <form class="form-inline" action="{{ route('companies.destroy', ['company'=> $company ]) }}"
                    method="post" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            </div>
        </div>

    </div>
</div>

@endsection