@extends('layouts.app')

@section('title', 'Task List')

@section('content')

<div class="row py-3">
  <h1> Tasks List </h1>
  <hr>
  @can('create', \App\Task::class)
  <a href="tasks/create" role="button" class="btn btn-success btn-lg">Add Task</a>
  @endcan
</div>
<div class="row">
  <table class="table  table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">User assigned</th>
        <th scope="col">Status</th>
        <th scope="col">Meeting</th>
        <th scope="col">Deadline</th>
        <th scope="col">Description</th>
        <th scope="col">Manage</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tasks as $task)
      {{-- {{ dd($task->user->id == auth()->user()->id) }} --}}
      @can('view',$task) 
      <tr>
        <th scope="row"> {{ $task->id }} </th>
        <td> {{ isset($task->user) ? $task->user->name : 'Not assigned'  }} </td>
        <td>
          <h5>
            <span class="badge badge-{{$task->statusColoring($task->status) }}">
              {{ $task->status }}
            </span>
          </h5>
        </td>
        <td>{{ isset($task->meeting) ? $task->meeting->title : 'None'}}</td>
        <td>{{ $task->deadline }}</td>
        <td> {{ $task->description }} </td>
        <td>
          <a class="btn btn-info" href="tasks/{{ $task->id }}" role="button">
            <i class="fa fa-eye"></i>
          </a>

          @can('delete', $task)
          @if($task->editable)
          <a class="btn btn-success" href="tasks/{{ $task->id }}/edit" role="button">
            <i class="fa fa-pencil"></i>
          </a>
          @endif
          <button class="btn btn-danger btn-sm deleteMe" data-task-id="{{ $task->id }}" role="button">
            <i class="fa fa-trash"></i>
          </button>
          @endcan
        </td>
      </tr>
      @endcan
      @endforeach
    </tbody>
  </table>


</div>

<script type="application/javascript">
  $(document).ready(function(){
    console.log('ready to work')
     $('.deleteMe').on('click', function(){
       console.log('Clicked: ' +'/tasks/' + $(this).attr('data-task-id'));
        $.ajax({
          url: "{{ asset('/tasks') }}/" + $(this).attr('data-task-id'),
          type: 'DELETE',
          headers: {
            "X-CSRF-TOKEN": "{{ csrf_token() }}",
          },
          success: function(){
            window.location.href = "tasks";
          },
          error: function(){
            window.location.href = "tasks";
          }
        });
     }) // Delete Me 
     
  });
</script>
@endsection