@extends('layouts.app')

@section('title', 'Task')

@section('content')

<div class="row py-3">
    <h1> Details for #{{ $task->id}} </h1>
    <hr>
</div>
<div class="row">
   
</div>
<div class="row">

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Details for #{{ $task->id}}
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Description: </strong>
                    {{ $task->description }}</li>
                <li class="list-group-item"><strong>Meeting: </strong>
                        {{ isset($task->meeting) ? $task->meeting->title : 'None' }}</li>
                <li class="list-group-item"><strong>User assigned:</strong>
                    {{ isset($task->user) ? $task->user->name : 'Not assigned'}}</li>
                <li class="list-group-item"><strong>Deadline: </strong>
                    {{ $task->deadline}}
                </li>
                <li class="list-group-item"><strong>Status:</strong>
                    <h5>
                        <span class="badge badge-{{$task->statusColoring($task->status) }}">
                            {{$task->status}}
                        </span>
                    </h5>
                </li>
            </ul>
            <div class="card-footer">
                    <a href="{{url('tasks')}}" class="btn btn-link pull-left">Back</a>
                    
                    @can('delete', $task)
                    @if($task->editable)
                    <a href="{{ route('tasks.edit', ['task'=> $task ]) }}" class="btn btn-success pull-left" role="button">
                            <i class="fa fa-pencil"></i>
                        </a>
                    @endif
                        <form class="form-inline" action="{{ route('tasks.destroy', ['task'=> $task ]) }}" method="post" enctype="multipart/form-data">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    @endcan
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(function () {
        var statuses = {
            'Not Assigned' : "secondary",
            'In Progress' : "warning",
            'Done' : "success",
            'Missed Deadline' : "danger",
        };
        // Set status for shown task
        $('.badge').addClass('badge-' + statuses["{{$task->status}}"]);
               
    });
</script>
@endsection