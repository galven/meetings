@extends('layouts.app')

@section('title', 'Edit Task')

@section('content')

<div class="row py-3">
    <h1> Edit Task </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('tasks.update', ['task'=> $task ]) }}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @include('tasks.form')
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>

@endsection