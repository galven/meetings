{{-- DatePicker https://tempusdominus.github.io/bootstrap-4/Usage/ --}}

@foreach ($errors->all() as $message)
<div class="alert alert-danger">
    <strong>Error!</strong>
    {{ $message }}
</div>
@endforeach
<div class="form-group">
    <label for="description">Description</label>
    <input type="text" class="form-control" name="description" id="description"
        value="{{ old('description') ?? $task->description }}" autofocus>
    @if($errors->first('description'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('description')}}
    </div>
    @endif
</div>

<div class="form-group">
    <label for="deadline">Deadline</label>
    {{-- jQuery DateTime Plugin --}}
    {{-- <input type="date" class="form-control" id="deadline" value="{{ old('deadline') ?? $task->deadline }}"> --}}
    {{-- @dump($task) --}}
    <div class="input-group date" data-target-input="nearest">
        <input type="text" class="form-control datetimepicker-input" data-target="#deadline"
            value="{{ old('deadline') ?? $task->deadline }}" name="deadline" id="deadline" />
        <div class="input-group-append" data-target="#deadline" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
    </div>
    @if($errors->first('deadline'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('deadline')}}
    </div>
    @endif
</div>

{{-- User to be assigned --}}
<div class="form-group">
    <label for="Users">
        Assign user for task:
    </label>
    <br>
    <select name="user_id" id="user_id" class="form-control">
        <option value="0">Not assigned</option>
        @foreach ($users as $user)
        @if(auth()->user()->company_id == $user->company_id)
        <option value="{{$user->id}}" {{$user->id == $task->user_id ? 'selected': ''}}>{{$user->name}}</option>
        @endif
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="mail_domain">Choose meeting</label>
    <select name="meeting_id" id="meeting_id" class="form-control custom-select">
        <option value="0">Select Meeting</option>
       @foreach ($meetings as $meeting)
         <option value="{{ $meeting->id }}"
            {{ isset($task->meeting) && $task->meeting->id === $meeting->id ? 'selected' : ''}}>
            {{ $meeting->title}} </option>
        @endforeach
    </select>
    @if($errors->first('meeting'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('meeting')}}
    </div>
    @endif
</div>

<div class="form-group">
    <label for="status">Status</label>
    <br>
    <select name="status" id="status" class="form-control">
        <option value="" disabled>Select task status</option>

        @foreach($task->statusOptions() as $statusOptionKey => $statusOptionValue)
        <option value="{{ $statusOptionKey }}" {{ $task->status === $statusOptionValue ? 'selected' : '' }}>
            {{ $statusOptionValue }}</option>
        @endforeach
    </select>
</div>
<a href="{{url('tasks')}}" class="btn btn-link">Cancel</a>
@csrf
<script type="application/javascript">
    $(function () {
            $('#deadline').datetimepicker({
               format:'Y-m-d H:i:s' // defaultDate: "{{ old('deadline') ?? $task->deadline}}"
            });
        });
</script>