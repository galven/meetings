@extends('layouts.app')

@section('title', 'Create New Task')

@section('content')

<div class="row py-3">
    <h1> Create New Task </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('tasks.store') }}" method="post" enctype="multipart/form-data">
        @include('tasks.form')
        <button type="submit" class="btn btn-success">Create</button>
    </form>
</div>

@endsection