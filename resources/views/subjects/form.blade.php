@foreach ($errors->all() as $message)
<div class="alert alert-danger">
    <strong>Error!</strong>
    {{ $message }}
</div>
@endforeach
<div class="form-group">
    <label for="name">Subject name</label>
    <input type="text" class="form-control" name="title" id="title" value="{{ old('title') ?? $subject->title }}" autofocus>
    @if($errors->first('title'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('title')}}
    </div>
    @endif
</div>

<div class="form-group">
    <label for="mail_domain">Choose meeting</label>
    <select name="meeting_id" id="meeting_id" class="form-control custom-select" >
        <option value="0">Select Meeting</option>
        @foreach ($meetings as $meeting)
        <option value="{{ $meeting->id }}" {{ isset($subject->meeting) && $subject->meeting->id === $meeting->id ? 'selected' : ''}}> {{ $meeting->title}} </option>    
        @endforeach
    </select>
    @if($errors->first('meeting'))
    <div class="alert alert-danger">
        <strong>Error!</strong>
        {{ $errors->first('meeting')}}
    </div>
    @endif
</div>
@if(Request::is('*/edit'))
<div class="form-group">
    <label for="registration_key">Subject Status</label>
    <select name="status" id="status" class="form-control">
            <option value="" disabled>Select subject status</option>
            @foreach($subject->statusOptions() as $statusOptionKey => $statusOptionValue)
            <option value="{{ $statusOptionKey }}" {{ $subject->status === $statusOptionValue ? 'selected' : '' }}>
                {{ $statusOptionValue }}</option>
            @endforeach
        </select>
    </select >
</div>
@endif


@csrf