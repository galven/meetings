@extends('layouts.app')

@section('title', 'Subject List')

@section('content')

<div class="row py-3">
  <h1> Subjects List </h1>
  <hr>
  @if(!$meetings->isEmpty())
  <a href="{{asset('/subjects/create')}}" role="button" class="btn btn-success btn-lg">Add subject</a>
  @else
  <div class="alert alert-danger" role="alert">
      Please create meetings in order to add subjects.
    </div>
  @endif
</div>
<div class="row">
  <table class="table  table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Status</th>
        <th scope="col">Meeting ID</th>
        <th scope="col">Manage</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($subjects as $subject)
      {{-- {{dd($subject)}} --}}
      <tr>
        <th scope="row"> {{$subject->id}} </th>
        <td>
          {{$subject->title}}
        </td>
        <td>
          {{ $subject->status}}
        </td>
        <td>
          {{isset($subject->meeting) ? $subject->meeting->title : 'No meeting'}}
        </td>
        <td>
            <a class="btn btn-info" href="subjects/{{ $subject->id }}" role="button">
              <i class="fa fa-eye"></i>
            </a>

          <a class="btn btn-success" href="subjects/{{ $subject->id }}/edit" role="button">
            <i class="fa fa-pencil"></i>

          </a>
        <button class="btn btn-danger btn-sm deleteMe" data-subject-id="{{ $subject->id }}" role="button">
            <i class="fa fa-trash"></i>
          </button>

        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <script type="application/javascript">
    $(document).ready(function(){
       $('.deleteMe').on('click', function(){
         console.log('Clicked: ' +'/subjects/' + $(this).attr('data-subject-id'));
          $.ajax({
            url: "{{url('subjects')}}/" + $(this).attr('data-subject-id'),
            type: 'DELETE',
            headers: {
              "X-CSRF-TOKEN": "{{ csrf_token() }}",
            },
            success: function(){
              window.location.href = "subjects";
            },
            error: function(){
              window.location.href = "subjects";
            }
          });
       }) 
      
    });
</script>
</div>

@endsection