@extends('layouts.app')

@section('title', 'Edit Subject')

@section('content')

<div class="row py-3">
    <h1> Edit subject </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('subjects.update', ['subject'=> $subject ]) }}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @include('subjects.form')
        <button type="submit" class="btn btn-success">Update</button>
    <a href="{{url('subjects')}}" class="btn btn-link">Cancel</a>
    </form>
</div>

@endsection