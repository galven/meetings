@extends('layouts.app')

@section('title', 'Subject')

@section('content')

<div class="row py-3">
    <h1> Details for subject #{{ $subject->id}} </h1>
    <hr>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Details for subject #{{ $subject->id}}
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Subject title</strong>
                    {{ $subject->title }}
                </li>
                <li class="list-group-item">
                    <strong>Subject on meeting</strong>
                    {{ $subject->meeting->title}}
                </li>
                <li class="list-group-item">
                    <strong>Status</strong>
                    {{ $subject->status }}
                </li>
            </ul>
            <div class="card-footer">
            <a href="{{url('subjects')}}" class="btn btn-link pull-left" role="button">Cancel</a>
            @if($subject->editable)
                <a href="{{ route('subjects.edit', ['subject'=> $subject ]) }}" class="btn btn-success pull-left"
                    role="button">
                    <i class="fa fa-pencil"></i>
                </a>
            @endif
                <form class="form-inline" action="{{ route('subjects.destroy', ['subject'=> $subject ]) }}"
                    method="post" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            </div>
        </div>

    </div>
</div>

@endsection