@extends('layouts.app')

@section('title', 'Create New Subject')

@section('content')

<div class="row py-3">
    <h1> Create New Subject </h1>
    <hr>
</div>
<div class="row">
    <form action="{{ route('subjects.store') }}" method="post" enctype="multipart/form-data">
        @include('subjects.form')
        <button type="submit" class="btn btn-success">Create</button>
    </form>
</div>

@endsection