<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->bigInteger('participant_id');
            $table->bigInteger('subject_id')->default(0);
            $table->string('manager');  // 
            $table->string('host');     // same person who's creating meeting
            $table->tinyInteger('status');
            $table->datetime('starts_at')->nullable();
            $table->datetime('finishs_at')->nullable();
            $table->tinyInteger('editable')->default(1);
            $table->tinyInteger('show')->default(1);
            $table->bigInteger('company_id')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
