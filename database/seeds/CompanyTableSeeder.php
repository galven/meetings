<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Company::truncate();

        Company::create([
            'name' => 'Demo Inc.',
            'mail_domain' => 'demo@inc.com',
            'registration_link' => (string) Str::uuid(),
        ]);
    }
}
