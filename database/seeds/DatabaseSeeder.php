<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        // $this->call(UsersTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        \App\User::truncate();
        $users = factory(App\User::class, 4)->create();
        foreach ($users as $user) {
            $user->roles()->attach(2);
            $user->update(['company_id' => 1]);
        }
        // Create Demo Admin
        $admin = $users[0];
        $admin->name = 'Admin';
        $admin->email = 'admin@inc.com';
        $admin->password = Hash::make('admin@inc.com');
        $admin->save();

        // Create Demo Manager
        $manager = $users[1];
        $manager->name = 'Manager';
        $manager->email = 'manager@inc.com';
        $manager->password = Hash::make('manager@inc.com');
        $manager->roles()->attach(3);
        $manager->save();

        // Create Demo User
        $user = $users[2];
        $user->name = 'User';
        $user->email = 'user@inc.com';
        $user->password = Hash::make('user@inc.com');
        $user->save();

        // factory(App\Subject::class, 3)->create();
        // factory->(App\Tasks::class, 10)->create(); // not working 
        $this->call(RolesTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
 
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
